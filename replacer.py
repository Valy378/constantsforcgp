import os
import sys
import fileinput
import re

import sys


with open('constants.txt', 'r') as file :
        constants = file.read()
        
constants = constants.split(" ")
constants.pop()
#print(constants)

with open('chromo.tex', 'r') as file :
    filedata = file.read()
#print(filedata)
#print(len(constants))
for i in range(len(constants)):
    filedata = filedata.replace('linear(x_'+str(i)+')', f'(x_{i} - ' + constants[i] + ')')
    filedata = filedata.replace('hyperbola(x_'+str(i)+')', '(\\frac{1}{'+ f'x_{i} - ' + constants[i] + '})')



filedata = re.sub('(_\d*)', lambda m: "_{" + m.group(1)[1:]+ '}', filedata)
    #filedata = filedata.replace('x_'+str(i), 'x_'+'{'+str(i)+'}')

#print(filedata)

with open('chromo_new.tex', 'w') as file:
  file.write(filedata)