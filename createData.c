#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "cgp.h"

#define NUMINPUTS 2
#define NUMOUTPUTS 1
#define NUMSAMPLES 100
#define INPUTRANGE 10.0



double drand(double low, double high)
{
	return ((double)rand() * (high - low)) / (double)RAND_MAX + low;
}

/*
	Returns 1/(x-2) + y
*/
double symbolicEq1(double x, double y) {
	return 1/(x-2) + y;
}


int simpleExample(void) {

	int i;

	struct dataSet *data = NULL;

	double inputs[NUMSAMPLES][NUMINPUTS];
	double outputs[NUMSAMPLES][NUMOUTPUTS];

	double inputTemp1;
	double inputTemp2;
	double outputTemp;

	for (i = 0; i < NUMSAMPLES; i++) {

		inputTemp1 = drand(-10, 10);
		inputTemp2 = drand(-10,10);
		outputTemp = symbolicEq1(inputTemp1, inputTemp2);

		inputs[i][0] = inputTemp1;
		inputs[i][1] = inputTemp2;
		outputs[i][0] = outputTemp;
	}

	data = initialiseDataSetFromArrays(NUMINPUTS, NUMOUTPUTS, NUMSAMPLES, inputs[0], outputs[0]);

	saveDataSet(data, "simpleExample.data");

	freeDataSet(data);

	return 0;
}


double symbolicEq2(double x_0, double x_1, double x_2, double x_3) {
	return x_0 + 1 / (x_1 - 2) + (x_2 - 5) + 1/(x_3 - 6);
}



int hardExample(void) {


	int i;

	struct dataSet *data = NULL;

	double inputs[100][4];
	double outputs[100][1];

	double inputTemp1, inputTemp2, inputTemp3, inputTemp4;
	double outputTemp;

	for (i = 0; i < 100; i++) {

		inputTemp1 = drand(-10, 10);
		inputTemp2 = drand(-10, 10);
		inputTemp3 = drand(-10, 10);
		inputTemp4 = drand(-10, 10);
		outputTemp = symbolicEq2(inputTemp1, inputTemp2, inputTemp3, inputTemp4);

		inputs[i][0] = inputTemp1;
		inputs[i][1] = inputTemp2;
		inputs[i][2] = inputTemp3;
		inputs[i][3] = inputTemp4;

		outputs[i][0] = outputTemp;
	}

	data = initialiseDataSetFromArrays(4, 1, 100, inputs[0], outputs[0]);

	saveDataSet(data, "hardExample.data");

	freeDataSet(data);

	return 0;
}


