#include <stdio.h>
#include <stdlib.h>
#include "cgp.h"

#define QWERY 14

double my_func(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return 1 / (inputs[0] - simpleConstant);
}

double my_func1(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return (inputs[0] - simpleConstant);
}

int main(int argc, char** argv) {

	//simpleExample();
	hardExample();

	struct parameters *params = NULL;
	struct dataSet *trainingData = NULL;
	struct chromosome *chromo = NULL;


	int numInputs = 4;
	int numNodes = 200;
	int numOutputs = 1;
	int nodeArity = 2;
	int numGens = 80000;
	double targetFitness = 0.01;
	int updateFrequency = 500;
	double maxMutationConst = 0.1;

	FILE * pFile;
	pFile = fopen("parameters.txt", "r");

	fscanf(pFile, "numInputs = %i;\n", &numInputs);
	fscanf(pFile, "numNodes = %i;\n", &numNodes);
	fscanf(pFile, "numOutputs = %i;\n", &numOutputs);
	fscanf(pFile, "nodeArity = %i;\n", &nodeArity);
	fscanf(pFile, "numGens = %i;\n", &numGens);
	fscanf(pFile, "targetFitness = %lf;\n", &targetFitness);
	fscanf(pFile, "updateFrequency = %i;\n", &updateFrequency);
	fscanf(pFile, "maxMutationConst = %lf;\n", &maxMutationConst);


	double* defaultSimpleConstants = malloc(numInputs * sizeof(double));

	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &defaultSimpleConstants[i]);
		printf("%lf ", defaultSimpleConstants[i]);
	}
	//defaultSimpleConstants[0] = 50000;
	//defaultSimpleConstants[1] = 0;
	//defaultSimpleConstants[2] = 0;
	//defaultSimpleConstants[3] = 0;
	fscanf(pFile, "\n", NULL);
	printf("\n");
	double* shiftForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &shiftForSigmoid[i]);
		printf("%lf ", shiftForSigmoid[i]);
	}
	//shiftForSigmoid[0] = -1;
	//shiftForSigmoid[1] = 1.5;
	//shiftForSigmoid[2] = 4.5;
	//shiftForSigmoid[3] = 5.5;
	fscanf(pFile, "\n", NULL);
	printf("\n");
	double* scaleForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &scaleForSigmoid[i]);
		printf("%lf ", scaleForSigmoid[i]);
	}

	params = initialiseParameters(numInputs, numNodes, numOutputs, nodeArity, maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);

	addNodeFunction(params, "add,sub,mul,div");
	addCustomNodeFunction(params, my_func, "hyperbola", 1);
	addCustomNodeFunction(params, my_func1, "linear", 1);

	setTargetFitness(params, targetFitness);

	setUpdateFrequency(params, updateFrequency);

	printParameters(params);

	// Note: you may need to check this path such that it is relative to your executable 
	//trainingData = initialiseDataSetFromFile("hardExample.data");

	char* train_filename;
	char* test_filename = NULL;

	char* output_chromo = NULL;
	char* output_constants = NULL;
	char* output_tex = NULL;
	if (argc == 3) {
		//printf("OK, I get two args");
		train_filename = argv[1];
		test_filename = argv[2];
		printf("%s", test_filename);

		trainingData = initialiseDataSetFromFile(train_filename);
	}
	else if(argc == 6){
		train_filename = argv[1];
		test_filename = argv[2];
		output_chromo = argv[3];
		output_constants = argv[4];
		output_tex = argv[5];
		
		trainingData = initialiseDataSetFromFile(train_filename);

	}
	else {
		trainingData = initialiseDataSetFromFile("hardExample.data");
	}

	chromo = runCGP(params, trainingData, numGens);

	
	printChromosome(chromo, 0);
	
	//saveChromosomeDot(chromo, 0, "chromo.dot");
	if (argc == 6) {
		saveConstants(chromo, output_constants);
		saveChromosome(chromo, output_chromo);
		saveChromosomeLatex(chromo, 0, output_tex);
	}
	else {
		saveChromosomeLatex(chromo, 0, "chromo.tex");
		saveConstants(chromo, "constants.txt");
	}
	
	//system("py replacer.py");

	if (argc == 3 || argc == 6) runOnTest(chromo, test_filename);

	freeDataSet(trainingData);
	freeChromosome(chromo);
	freeParameters(params);

	return 0;
}