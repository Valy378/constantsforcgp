
import os
import sys
import fileinput
import re

import sys

input_file = sys.argv[1]
constants_file = sys.argv[2]

with open(constants_file, 'r') as file:
    constants = file.read()

constants = constants.split(" ")
constants.pop()

with open(input_file, 'r') as file :
    filedata = file.read()

for i in range(len(constants)):
    filedata = filedata.replace('linear(x_'+str(i)+')', f'(x_{i} - ' + constants[i] + ')')
    filedata = filedata.replace('hyperbola(x_'+str(i)+')', '(\\frac{1}{'+ f'x_{i} - ' + constants[i] + '})')


def repl(m):
    number = m.group(1)[1:]
    if int(number) < 18:
        return "_{snp "+number+" }"
    
    tmp = int(number)-18
    num = tmp//5
    if tmp % 5 == 0:
        return "_{dl "+str(num)+" }"
    if tmp % 5 == 1:
        return "_{rain "+str(num)+" }"
    if tmp % 5 == 2:
        return "_{srad "+str(num)+" }"
    if tmp % 5 == 3:
        return "_{tmax "+str(num)+" }"
    if tmp % 5 == 4:
        return "_{tmin "+str(num)+" }"

filedata = re.sub('(_\d*)', repl, filedata)

with open(input_file+'chromo_notation.tex', 'w') as file:
  file.write(filedata)