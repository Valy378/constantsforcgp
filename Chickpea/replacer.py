import os
import sys
import fileinput
import re

import sys

input_file = sys.argv[1]
constants_file = sys.argv[2]

with open(constants_file , 'r') as file :
        constants = file.read()
        
constants = constants.split(" ")
constants.pop()
#print(constants)

with open(input_file, 'r') as file :
    filedata = file.read()
#print(filedata)
#print(len(constants))
for i in range(len(constants)):
    filedata = filedata.replace('linear(x_'+str(i)+')', f'(x_{i} - ' + constants[i] + ')')
    filedata = filedata.replace('hyperbola(x_'+str(i)+')', '(\\frac{1}{'+ f'x_{i} - ' + constants[i] + '})')



filedata = re.sub('(_\d*)', lambda m: "_{" + m.group(1)[1:]+ '}', filedata)
    #filedata = filedata.replace('x_'+str(i), 'x_'+'{'+str(i)+'}')

#print(filedata)

with open(input_file+'chromo_with_constants.tex', 'w') as file:
  file.write(filedata)