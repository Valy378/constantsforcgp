#include <stdio.h>
#include <stdlib.h>
#include "cgp.h"

#define QWERY 14

double my_func(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return 1 / (inputs[0] - simpleConstant);
}

double my_func1(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return (inputs[0] - simpleConstant);
}

int main(int argc, char** argv) {



	struct parameters *params = NULL;
	struct dataSet *trainingData = NULL;
	struct chromosome *chromo = NULL;


	int numInputs = 4;
	int numNodes = 200;
	int numOutputs = 1;
	int nodeArity = 2;
	int numGens = 80000;
	double targetFitness = 0.01;
	int updateFrequency = 500;
	double maxMutationConst = 0.1;

	FILE * pFile;
	pFile = fopen("parameters.txt", "r");

	fscanf(pFile, "numInputs = %i;\n", &numInputs);
	fscanf(pFile, "numNodes = %i;\n", &numNodes);
	fscanf(pFile, "numOutputs = %i;\n", &numOutputs);
	fscanf(pFile, "nodeArity = %i;\n", &nodeArity);
	fscanf(pFile, "numGens = %i;\n", &numGens);
	fscanf(pFile, "targetFitness = %lf;\n", &targetFitness);
	fscanf(pFile, "updateFrequency = %i;\n", &updateFrequency);
	fscanf(pFile, "maxMutationConst = %lf;\n", &maxMutationConst);


	double* defaultSimpleConstants = malloc(numInputs * sizeof(double));

	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &defaultSimpleConstants[i]);
		printf("%lf ", defaultSimpleConstants[i]);
	}

	fscanf(pFile, "\n", NULL);
	printf("\n");
	double* shiftForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &shiftForSigmoid[i]);
		printf("%lf ", shiftForSigmoid[i]);
	}

	fscanf(pFile, "\n", NULL);
	printf("\n");
	double* scaleForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &scaleForSigmoid[i]);
		printf("%lf ", scaleForSigmoid[i]);
	}

	//arams = initialiseParameters(numInputs, numNodes, numOutputs, nodeArity, maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);

	//addNodeFunction(params, "add,sub,mul,div");
	//addCustomNodeFunction(params, my_func, "hyperbola", 1);
	//addCustomNodeFunction(params, my_func1, "linear", 1);

	//setTargetFitness(params, targetFitness);

	//setUpdateFrequency(params, updateFrequency);

	//printParameters(params);
	

	chromo = initialiseChromosomeFromFile("0_chromo.chromo", maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);
	loadConstants(chromo, "0_constants.txt");
	runOnTest(chromo, "validation.data");

	freeDataSet(trainingData);
	freeChromosome(chromo);
	freeParameters(params);

	return 0;
}