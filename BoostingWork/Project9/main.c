#include <stdio.h>
#include <stdlib.h>
#include "cgp.h"


double maximum(double num[], int size)
{
	int i;
	double max;
	max = num[0];

	for (i = 1; i < size; i++) {
		if (num[i] > max)
			max = num[i];
	}
	return max;
}


double my_func(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return 1 / (inputs[0] - simpleConstant);
}

double my_func1(const int numInputs, const double *inputs, const double *connectionWeights, double simpleConstant) {
	return (inputs[0] - simpleConstant);
}


double SS_tot(double a[], int n)
{
	double sum = 0;
	for (int i = 0; i < n; i++) sum += a[i];

	double mean = (double)sum /(double)n;

	double sqDiff = 0;
	for (int i = 0; i < n; i++) sqDiff += (a[i] - mean) * (a[i] - mean);
	return sqDiff;
}

double SS_res(double a[], int n)
{
	double sum = 0;
	for (int i = 0; i < n; i++) sum += a[i]*a[i];
	return sum;
}



int main(int argc, char** argv) {



	struct parameters *params = NULL;
	struct dataSet *trainingData = NULL;
	struct chromosome *chromo = NULL;


	int numInputs = 4;
	int numNodes = 200;
	int numOutputs = 1;
	int nodeArity = 2;
	int numGens = 80000;
	double targetFitness = 0.01;
	int updateFrequency = 500;
	double maxMutationConst = 0.1;

	FILE * pFile;
	pFile = fopen("parameters.txt", "r");

	fscanf(pFile, "numInputs = %i;\n", &numInputs);
	fscanf(pFile, "numNodes = %i;\n", &numNodes);
	fscanf(pFile, "numOutputs = %i;\n", &numOutputs);
	fscanf(pFile, "nodeArity = %i;\n", &nodeArity);
	fscanf(pFile, "numGens = %i;\n", &numGens);
	fscanf(pFile, "targetFitness = %lf;\n", &targetFitness);
	fscanf(pFile, "updateFrequency = %i;\n", &updateFrequency);
	fscanf(pFile, "maxMutationConst = %lf;\n", &maxMutationConst);


	double* defaultSimpleConstants = malloc(numInputs * sizeof(double));

	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &defaultSimpleConstants[i]);
		//printf("%lf ", defaultSimpleConstants[i]);
	}

	fscanf(pFile, "\n", NULL);
	//printf("\n");
	double* shiftForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &shiftForSigmoid[i]);
		//printf("%lf ", shiftForSigmoid[i]);
	}

	fscanf(pFile, "\n", NULL);
	//printf("\n");
	double* scaleForSigmoid = malloc(numInputs * sizeof(double));
	for (int i = 0; i < numInputs; ++i) {
		fscanf(pFile, "%lf;\n", &scaleForSigmoid[i]);
		//printf("%lf ", scaleForSigmoid[i]);
	}

	//arams = initialiseParameters(numInputs, numNodes, numOutputs, nodeArity, maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);

	//addNodeFunction(params, "add,sub,mul,div");
	//addCustomNodeFunction(params, my_func, "hyperbola", 1);
	//addCustomNodeFunction(params, my_func1, "linear", 1);

	//setTargetFitness(params, targetFitness);

	//setUpdateFrequency(params, updateFrequency);

	//printParameters(params);



	int begin = -1;
	int end = 18;
	//char test_filename[100]= "0_test.data";
	char* test_filename = NULL;
	char* train_filename = NULL;
	if (argc == 3) {
		test_filename = argv[1];
		train_filename = argv[2];
		begin = -1;
		end = 18;
	}
	else if (argc == 5) {
		test_filename = argv[1];
		train_filename = argv[2];
		begin = strtol(argv[3], NULL, 10);
		end = strtol(argv[4], NULL, 10);
	}
	else
	{
		printf("test.data train.data\n");
		exit;
	}



	double error = 0;
	struct dataSet *data;
	data = initialiseDataSetFromFile(test_filename);
	double* errors = (double*)calloc(getDataSetNumSamples(data), sizeof(double));

	for (int i = begin; i < end; i++) {
		char const_filename[100];
		char chromo_filename[100];

		snprintf(const_filename, 100, "%s_const%02d.txt", train_filename, i+1); 
		snprintf(chromo_filename, 100, "%s_chromo%02d.chromo", train_filename, i+1); 

		chromo = initialiseChromosomeFromFile(chromo_filename, maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);
		loadConstants(chromo, const_filename);

		getResult(data, errors, chromo, i);

		freeChromosome(chromo);
	}

	double* real = (double*)calloc(getDataSetNumSamples(data), sizeof(double));
	for (int i = 0; i < getDataSetNumSamples(data); i++)
		real[i] = getDataSetSampleOutput(data, i, 0);
	double SS_tot_ = SS_tot(real, getDataSetNumSamples(data));

	for (int i = 0; i < getDataSetNumSamples(data); i++)
		errors[i] = fabs(errors[i] - getDataSetSampleOutput(data, i, 0));

	double SS_res_ = SS_res(errors, getDataSetNumSamples(data));
	double sum_errors = 0;
	for (int i = 0; i < getDataSetNumSamples(data); i++)
		sum_errors += errors[i];

	printf("%lf\n", sum_errors);

	printf("%lf\n", 1- SS_res_/SS_tot_);
	
	printf("%lf\n", maximum(errors, getDataSetNumSamples(data)));

	

	//chromo = initialiseChromosomeFromFile("0_chromo.chromo", maxMutationConst, defaultSimpleConstants, shiftForSigmoid, scaleForSigmoid);
	//loadConstants(chromo, "0_constants.txt");
	//runOnTest(chromo, "validation.data");

	freeDataSet(data);
	//freeChromosome(chromo);
	//freeParameters(params);

	return 0;
}